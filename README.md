Features:
1. Get audio from video file
2. Find silence parts of audio file and store non-silence parts into a WAV file
3. Convert each audio to text
4. Build a SRT file as video subtitle

Install:
1. Clone project using "git clone https://gitlab.com/ali1936/dubberenfa.git"
2. Run main.py file

Usage:
- This project needs a good internet connection and VPN.
- You can copy your video in dubberenfa\src\files
- Modify every path in main.py or leave It to defaults

- Dependencies:
1. pyaudio
2. speech_recognition
3. pydub
4. moviepy
6. FFmpeg
