import pyaudio
import speech_recognition as sr
from pydub import AudioSegment, silence

audioPath = "dubberenfa\\files\\NLP.wav"
r = sr.Recognizer()


def detectSilenceInAudio(audioPath) -> list:
    myaudio = AudioSegment.from_file(audioPath)
    dBFS = myaudio.dBFS
    silences = silence.detect_silence(
        myaudio, min_silence_len=1000, silence_thresh=dBFS-16)
    silences = [((start), (stop))
                for start, stop in silences]  # in sec
    # for index in silences:
    #     print("start silenc : ")
    #     print(index[0])
    #     print("end silenc : ")
    #     print(index[1])
    return silences
#----------------------------------------------------------------


def getAudioFromMicrophone():
    with sr.AudioFile(audioPath) as source:
        audio = r.record(source)
        return audio
#----------------------------------------------------------------


def getAudioFromAudioFile(audioPath):
    with sr.AudioFile(audioPath) as source:
        audio = r.record(source)
        return audio
#----------------------------------------------------------------


def getTextFromAudio(audioPath):
    audio = AudioSegment.from_file(audioPath)
    start_time = 0
    audio_index = 0
    silences = detectSilenceInAudio(audioPath)
    non_silences = []
    sentences = []
    for index in silences:

        non_silences.append([(start_time), (index[0])])

        extract = audio[start_time:index[0]]

        start_time = index[1]

        if(len(non_silences) == len(silences)):
            non_silences.append([start_time, len(audio)])

        extract.export(str(audio_index)+'.wav', format="wav")
        # try:
        #     text = r.recognize_google(getAudioFromAudioFile(audioPath))
        #     print("Audio to text is : \n " + text)
        #     return text
        # except sr.UnknownValueError:
        #     print("UnknownValueError")
        audio_index += 1
    audio_index = 0
    for i in range(0, len(non_silences)-1):
        sentences.append(speechToText(str(i)+".wav"))
    return sentences, non_silences

#----------------------------------------------------------------


def speechToText(audioPath):
    try:
        print("\t \t \t Proccesig Audio...\n")
        text = r.recognize_google(
            getAudioFromAudioFile(audioPath))
        print(str(audioPath)+"to text is : \n " + text)
        print("\n \t \t \t Done!")
        return text
    except sr.UnknownValueError:
        print("UnknownValueError")
        return "UnknownValue"
