import nltk
from nltk import *
from nltk.corpus import webtext


def SentenceTokenize(text):
    #Train a tokenizer
    sent_tokenize(text)  # this tokenizer is not trained
    nltk.download("webtext")  # download webtext for train a new tokenizer
    # use a text from webtext for train a new tokenizer
    t = webtext.raw("overheard.txt")
    from nltk.tokenize import PunktSentenceTokenizer
    # use PunktSentenceTokenizer for train a new tokenizer
    my_tokenizer = PunktSentenceTokenizer(t)
    return my_tokenizer.tokenize(text)


def WordTokenize(text):
    from nltk.tokenize import WordPunctTokenizer
    #Use WordPunctTokenizer for tokenize words
    tokenizer = WordPunctTokenizer()
    return tokenizer.tokenize(text)
