import moviepy.editor as editor

#Get videoPath and get the audio of the video and store it in audioPath


def getAudioOfVideo(videoPath, audioPath):
    video = editor.VideoFileClip(videoPath)
    audio = video.audio
    audio.write_audiofile(audioPath)
    print("Done! \n Audio extracted to "+audioPath)
