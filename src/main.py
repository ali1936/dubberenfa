
import VideoEditor
import AudioEditor
import TextEditor
import Tokenizer

videoPath = "ww.mp4"
audioPath = "NL.wav"
srtPath = "subtitle.srt"

#VideoEditor.getAudioOfVideo(videoPath, audioPath)
silences = AudioEditor.detectSilenceInAudio(audioPath)
sentences, non_silences = AudioEditor.getTextFromAudio(audioPath)
TextEditor.getSRTFileFromText(sentences, non_silences, srtPath)
#print(Tokenizer.SentenceTokenize(text))
