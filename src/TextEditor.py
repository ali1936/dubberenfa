import datetime

start_time = datetime.datetime(100, 1, 1, 0, 0, 0, 0)
block_num = "1"

# getSRTFileFromText() => Get list of sentences and non-silence part of audio
# and convert it to SRT file


def getSRTFileFromText(sentences: list, non_silences: list, srtPath: str):
    block_num = 1
    current_time = start_time
    for index in range(0, len(non_silences)-1):
        # Calculate the silence time between non_silence parts
        if(index > 0):
            add_silent_time = non_silences[index][0] - non_silences[index-1][1]
            tmp_change_time = datetime.timedelta(seconds=int(add_silent_time / 1000),
                                                 milliseconds=add_silent_time % 1000)
            current_time = current_time+tmp_change_time
        # Calculate the time that must add to end_time in second
        time_add = non_silences[index][1] - non_silences[index][0]
        # Calculate the time that must add to end_time for sentence durution in datetime.datetime
        time_change = datetime.timedelta(seconds=int(time_add / 1000),
                                         milliseconds=time_add % 1000)
        # end_time of current sentence
        end_time = current_time + time_change
        # Convert start time and end time of sentence to write into subtitle file
        str_current_time = timeToStrig(current_time)
        str_end_time = timeToStrig(end_time)
        # Edit subtitle file and add all sentences with time
        with open("subtitle.srt", "a") as text:
            text.write("\n")
            text.write(str(block_num))
            text.write("\n")
            text.write(str_current_time)
            text.write("-->")
            text.write(str_end_time)
            text.write("\n")
            text.write(str(sentences[index]))
        # Goto next line of subtitle
        block_num = block_num + 1

        # Depricated code
        # if(index < len(non_silences)-1):
        #     current_time = end_time + \
        #         datetime.timedelta(
        #             0, non_silences[index+1][0]-non_silences[index][1])

        # Set start time of new subtitle new line
        current_time = end_time

# timeToStrig() => Convert Time to subtitle string format HH:MM:SS,MIMIMI


def timeToStrig(time: datetime.datetime) -> str:
    hour = int(time.hour)
    minute = int(time.minute)
    second = int(time.second)
    milliseconds = int(time.microsecond/1000)
    strHour = checkTimeLen(hour)
    strMin = checkTimeLen(minute)
    strSec = checkTimeLen(second)
    strMilSec = checkTimeLen(milliseconds)
    return strHour+":"+strMin+":"+strSec+","+strMilSec


# checkTimeLen() => Get one of time properties and convert it to right format


def checkTimeLen(prop: int) -> str:
    returnStr = ""
    if(len(str(prop)) == 1):
        returnStr = "0"
        returnStr = returnStr + (str(prop))
        return returnStr
    else:
        returnStr = (str(prop))
        return returnStr

# Depricated


def time_addition(sentence, current_time):
    time_add = (len(sentence.split()))*0.5
    end_time = current_time+datetime.timedelta(0, time_add)
    str_current_time = str(current_time.time())
    str_end_time = str(end_time.time())
    with open("subtitle.srt", "a") as text:
        text.write(str(block_num))
        text.write("\n")
        text.write(str_current_time)
        text.write("-->")
        text.write(str_end_time)
        text.write("\n")
        text.write(sentence)
#time_addition(test_sentence,start_time)
